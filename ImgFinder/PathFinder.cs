﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ImgFinder
{
    class PathFinder
    {
        private const string imgPattern = @"<img [^>]*src=[""|']([^>|^""|']*)[""|']";

        private const string shortPattern = @"<img ";

        public List<string> GetImagePaths(string input)
        {
            List<string> paths = new List<string>();
            Regex r = new Regex(imgPattern, RegexOptions.IgnoreCase);
            Match m = r.Match(input);
            while (m.Success)
            {
                paths.Add(m.Groups[1].Value);
                m = m.NextMatch();
            }

            //необязательная часть проверки того, что все теги img были обработаны
            int checkCount = FindImgTagCount(input);
            if (paths.Count == checkCount)
            {
                Console.WriteLine("Были найдены все картинки");
            }
            else
            {
                Console.WriteLine($"Должно быть {checkCount} картинок , а найдено {paths.Count}");
            }
            return paths;
        }


        private int FindImgTagCount(string url)
        {

            int count = 0;
            int position = 0;
            while ((position = url.IndexOf(shortPattern, position)) != -1)
            {
                count++;
                position += shortPattern.Length;
            }

            return count;
        }
    }
}
