﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ImgFinder
{
    class ImgPicker
    {
        private readonly PathFinder _finder;
        private readonly HtmlDownloader _downloader;

        public ImgPicker(HtmlDownloader downloader, PathFinder finder)
        {
            _finder = finder;
            _downloader = downloader;
        }

        public void PrintHtml(string url)
        {
            string html = _downloader.GetHtml(url);
            Console.WriteLine(html);
        }

        public void GetImages(string url)
        {
            try
            {
                Uri uriMain = new Uri(url);
                string host = uriMain.Host;
                string scheme = uriMain.Scheme;
                string html = _downloader.GetHtml(url);
                foreach (var path in _finder.GetImagePaths(html))
                {
                    string absPath = path.StartsWith('/') ? $"{scheme}://{host}{path}" : path;
                    _downloader.DownloadFile(absPath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Произошла ошибка {e.Message}");
            }
        }
    }
}
