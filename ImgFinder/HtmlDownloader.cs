﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ImgFinder
{
    class HtmlDownloader
    {
        private readonly string imgDownloadsDir;

        public HtmlDownloader(string path)
        {
            imgDownloadsDir = path;
        }

        public string GetHtml(string url)
        {
            try
            {
                var request = WebRequest.Create(url);
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Не удалось прочитать html страницу, ошибка: {e.Message}");
                return string.Empty;
            }
        }

        public void DownloadFile(string url)
        {
            string fileName = url.TrimStart("https://".ToCharArray()).TrimStart("http://".ToCharArray()).Replace('/', '_');
            WebClient client = new WebClient();
            try
            {
                client.DownloadFile(url, Path.Combine(imgDownloadsDir, fileName));
                Console.WriteLine($"Успешно загружен файл {fileName} с адреса : {url}");

            }
            catch (Exception e)
            {
                Console.WriteLine($"Загрузка файла с адреса {url} не удалась , ошибка: {e.Message}");
            }

        }
    }
}
