﻿using System;
using System.IO;

namespace ImgFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            string downloadsDir = Path.Combine(Directory.GetCurrentDirectory(), "img_downloads");
            if (!Directory.Exists(downloadsDir))
            {
                Directory.CreateDirectory(downloadsDir);
            }

            var downloader = new HtmlDownloader(downloadsDir);
            var imgPicker = new ImgPicker(downloader, new PathFinder());

            Console.WriteLine("Введите url");

            string url = Console.ReadLine();
            imgPicker.GetImages(url);
        }
    }
}
